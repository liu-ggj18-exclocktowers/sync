# This is NOT the main repository for Sync.

**We used this one only for the graphics resources**

Switch to [proper game repository](https://gitlab.com/liu-ggj18-exclocktowers/UI-implementation)

## About
Sync is a cooperative game for two players. One plays as a decryption operator and the other plays as an information receiver and communicator. The main goal is to decode and transmit messages to the Bresnvick military main base.

## How to play
The operator manages the computer screen.
The receiver manages the headphones.
