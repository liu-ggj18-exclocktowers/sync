import pygame
import time
import math

# define display surface			
#W, H = 1280, 720
W, H = 750, 550

# define some colors
COLOR_BG = (33, 177, 177, 255)
COLOR_CURVE = (90, 255, 240, 255)
COLOR_BEAT = (0, 0, 0, 0)
NUMBER_OF_TONES = 24

# initialise display
pygame.init()
CLOCK = pygame.time.Clock()
DS = pygame.display.set_mode((W, H))
DS.fill(COLOR_BG)
FPS = 3000.00
MSPF = 1.00 / FPS

# tone range
position_range = [H - i * H // (NUMBER_OF_TONES-1) for i in range(0, NUMBER_OF_TONES)]
current_tone = NUMBER_OF_TONES // 2

beat_dot_size = 10
beat_dot_x = W - beat_dot_size

dot_size = 3
dot_x = W - dot_size
dot_y = 500 #position_range[current_tone]
sin_x = 0

freq = 0.001

def draw():
	DS.fill(COLOR_BG, (W - 1 , 0, dot_size * 2, H))
	pygame.draw.circle(DS, COLOR_CURVE, (dot_x, 360 + math.floor(math.sin(sin_x*dot_y*freq)*100)), dot_size, 0)
	#pygame.draw.circle(DS, COLOR_CURVE, (dot_x, 360 +  math.floor(math.sin(sin_x)*dot_y*0.1)), dot_size, 0)
	DS.blit(DS, (0, 0), (1, 0, W - 1, H))

def update_sin_x(x):
	# if x >= math.pi:
	# 	x = - math.pi
	# else:
	# 	x = x + 0.1
	return x + 0.1

def update():
	testTime = time.time()
	if testTime - startTime >= MSPF:
		startTime += MSPF
		pygame.display.update()

exit = False
startTime = time.time()

# main loop
while True:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			exit = True
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				exit = True
			elif event.key == pygame.K_UP:

				dot_y_old = dot_y

				inc = 30
				dot_y += inc

				#if math.cos(dot_y_old * sin_x * freq) < 0:

				sin_x_inc = (math.asin(math.sin(dot_y_old * sin_x * freq)) - (dot_y * sin_x * freq) ) / (dot_y * freq)

				sin_x += sin_x_inc 

			elif event.key == pygame.K_DOWN:
				dot_y_old = dot_y

				inc = -30
				dot_y += inc
				#if math.cos(dot_y_old * sin_x * freq) < 0:
				
				sin_x_inc = (math.asin(math.sin(dot_y_old * sin_x * freq)) - (dot_y * sin_x * freq)) / (dot_y * freq)

				sin_x += sin_x_inc 

			elif event.key == pygame.K_SPACE:
				pygame.draw.circle(DS, COLOR_BEAT, (beat_dot_x, 20), beat_dot_size, 0)
				DS.blit(DS, (0, 0), (1, 0, W - 1, H))

	if exit: break
	
	#my = pygame.mouse.get_pos()[1]
	
	draw()

	testTime = time.time()
	if testTime - startTime >= MSPF:
		startTime += MSPF
		sin_x = update_sin_x(sin_x)
		pygame.display.update()

pygame.quit()